# Kiến trúc sư Phan Đình Kha

Kiến trúc sư Phan Đình Kha với 15 năm kinh nghiệm, hơn 1000 công trình, đoạt giải nhất kiến trúc quốc gia 2010. Các thiết kế của anh như Cảng hàng không Liên Khương – Đà Lạt, Bệnh viện Phú Mỹ (TP.HCM), khách sạn 4 sao Phú Mỹ – Sóc Trăng. Nhà hàng Miss Sài Gòn. Homestay Đà Lạt của vợ chồng danh hài Trường Giang – Nhã Phương.
Địa chỉ: 122/1 Phổ Quang, Phường 9, Quận Phú Nhuận, TPHCM
SDT: 0989149805

https://kientrucsuvietnam.vn/phan-dinh-kha/
https://myspace.com/phandinhkha
https://www.flickr.com/people/195520373@N03/
